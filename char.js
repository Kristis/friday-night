$(document).ready(function() {
		var c1 = document.getElementById("myPieChart1");
		var myPieChart1 = new Chart(c1, {
    type: 'pie',
    data: {
    labels: [
        "Red",
        "Blue",
        "Yellow"
    ],
    datasets: [
        {
            data: [30, 5, 10],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
				  }]
		
		
		}
		
});

		var c2 = document.getElementById("myPieChart2");
		var myPieChart2 = new Chart(c2, {
    type: 'pie',
    data: {
    labels: [
        "Red",
        "Blue",
        "Yellow"
    ],
    datasets: [
        {
            data: [30, 5, 10],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
				  }]
		
		
		}
		
});
		
		var c3 = document.getElementById("myPieChart3");
		var myPieChart3 = new Chart(c3, {
    type: 'pie',
    data: {
    labels: [
        "Red",
        "Blue",
        "Yellow"
    ],
    datasets: [
        {
            data: [30, 5, 10],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
				  }]
		
		
		}
		
});
	
		var ctx = document.getElementById("myChart");
		var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Blue", "Yellow", "Green", "Purple"],
        datasets: [{
            label: 'Data',
            data: [19, 3, 5, 2],
            backgroundColor: [
                
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
                
            ],
            borderColor: [
                
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
               
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
		var ctx = document.getElementById("myChart1");
		var myChart1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Blue", "Yellow", "Green", "Purple"],
        datasets: [{
            label: 'Data',
            data: [19, 3, 5, 2],
            backgroundColor: [
                
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
                
            ],
            borderColor: [
                
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
               
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
		  }
});
});